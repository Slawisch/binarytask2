﻿using System;
using System.Threading;
using RabbitMQ.Wrapper;

namespace Pinger
{
    class Program
    {
        private static QueueWorker _queueWorker;
        static void Main()
        {
            _queueWorker = new QueueWorker();

            _queueWorker.SendMessageToQueue(Queues.Pong, "ping");

            _queueWorker.ListenQueue(Queues.Ping, (msg) =>
            {
                Console.WriteLine(DateTime.Now + " : " + msg);
                Thread.Sleep(2500);
                _queueWorker.SendMessageToQueue(Queues.Pong, "ping");
            });

            Console.ReadLine();
        }
    }
}
