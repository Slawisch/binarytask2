﻿using System;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

namespace RabbitMQ.Wrapper
{
    public class QueueWorker : IDisposable
    {
        private readonly IConnection _connection;
        private readonly IModel _channel;
        public QueueWorker()
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(
                queue: "ping_queue",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null
            );
            _channel.QueueDeclare(
                queue: "pong_queue",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null
            );
        }
        public void ListenQueue(Queues queueType, Action<string> listenAction)
        {
            var consumer = new EventingBasicConsumer(_channel);

            consumer.Received += (_, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);

                listenAction(message);
                _channel.BasicAck(ea.DeliveryTag, false);
            };

            switch (queueType)
            {
                case Queues.Ping:
                {
                    _channel.BasicConsume(queue: "ping_queue",
                        autoAck: false,
                        consumer: consumer);
                    break;
                }
                case Queues.Pong:
                {
                    _channel.BasicConsume(queue: "pong_queue",
                        autoAck: false,
                        consumer: consumer);
                    break;
                }
            }
            
        }

        public void SendMessageToQueue(Queues queueType, string message)
        {

            var body = Encoding.UTF8.GetBytes(message);

            switch (queueType)
            {
                case Queues.Ping:
                {
                    _channel.BasicPublish(
                        exchange: "",
                        routingKey: "ping_queue",
                        basicProperties: null,
                        body: body
                    );
                    break;
                }
                case Queues.Pong:
                {
                    _channel.BasicPublish(
                        exchange: "",
                        routingKey: "pong_queue",
                        basicProperties: null,
                        body: body
                    );
                    break;
                }
            }

        }

        public void Dispose()
        {
            _connection.Dispose();
            _channel.Dispose();
        }
    }
}
