﻿using System;
using System.Threading;
using RabbitMQ.Wrapper;

namespace Ponger
{
    class Program
    {
        private static QueueWorker _queueWorker;
        static void Main()
        {
            _queueWorker = new QueueWorker();

            _queueWorker.ListenQueue(Queues.Pong, (msg) =>
            {
                Console.WriteLine(DateTime.Now + " : " + msg);
                Thread.Sleep(2500);
                _queueWorker.SendMessageToQueue(Queues.Ping, "pong");
            });

            Console.ReadLine();
        }

    }
}
